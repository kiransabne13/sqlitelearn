package me.codeandroid.sqllearn;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class AddActivity extends AppCompatActivity {

    private Button buttonAdd, button20;
    int sum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        buttonAdd = findViewById(R.id.buttonAdd);
        button20 = findViewById(R.id.button20);

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sum = sum + 10;
                System.out.println(sum);
            }
        });

        button20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sum = sum + 20;
                Log.d("20", String.valueOf(sum));
            }
        });

        Log.d("Add : ", String.valueOf(sum));



    }
}
