package me.codeandroid.sqllearn.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import me.codeandroid.sqllearn.database.model.Client;

public class DatabaseHelper extends SQLiteOpenHelper{

    Context context;

    private static final String DATABASE_NAME = "Client.db";

    private static final int DATABASE_VERSION = 1;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }
// start with create table
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(Client.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Client.TABLE_NAME );
        onCreate(sqLiteDatabase);
    }


    public long insert(String nameValue, String address1, String address2, String address3, String mobile, String phone, String s, String s1, String s2, String s3, String s4, String s5) {

        //get Writeable Database;

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(Client.COLUMN_NAME, nameValue);
        values.put(Client.COLUMN_ADDRESS1, address1);
        values.put(Client.COLUMN_ADDRESS2, address2);
        values.put(Client.COLUMN_ADDRESS3, address3);
        values.put(Client.COLUMN_MOBILE, mobile);
        values.put(Client.COLUMN_PHONENO, phone);
        values.put(Client.COLUMN_CODE, s);
        values.put(Client.COLUMN_PERMIT, s1);
        values.put(Client.COLUMN_EXPIRY, s2);
        values.put(Client.COLUMN_CARD, s3);
        values.put(Client.COLUMN_ONLINE, s4);
        values.put(Client.COLUMN_COMMISSION, s5);

        long id = db.insert(Client.TABLE_NAME, null, values);

        db.close();

        return id;

    }

    public List<Client> getAllClients(){

        List<Client> clients = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Client.TABLE_NAME + " ORDER BY " +
                Client.COLUMN_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Client client = new Client();
                client.setId(cursor.getInt(cursor.getColumnIndex(Client.COLUMN_ID)));
                client.setName(cursor.getString(cursor.getColumnIndex(Client.COLUMN_NAME)));
                client.setAddress1(cursor.getString(cursor.getColumnIndex(Client.COLUMN_ADDRESS1)));
                client.setAddress2(cursor.getString(cursor.getColumnIndex(Client.COLUMN_ADDRESS2)));
                client.setAddress3(cursor.getString(cursor.getColumnIndex(Client.COLUMN_ADDRESS3)));
                client.setMobile(cursor.getString(cursor.getColumnIndex(Client.COLUMN_MOBILE)));
                client.setCode(cursor.getString(cursor.getColumnIndex(Client.COLUMN_CODE)));
                client.setPhoneno(cursor.getString(cursor.getColumnIndex(Client.COLUMN_PHONENO)));
                client.setPermit(cursor.getString(cursor.getColumnIndex(Client.COLUMN_PERMIT)));
                client.setExpiry(cursor.getString(cursor.getColumnIndex(Client.COLUMN_EXPIRY)));
                client.setCard(cursor.getString(cursor.getColumnIndex(Client.COLUMN_CARD)));
                client.setOnline(cursor.getString(cursor.getColumnIndex(Client.COLUMN_ONLINE)));
                client.setCommission(cursor.getString(cursor.getColumnIndex(Client.COLUMN_COMMISSION)));

                clients.add(client);
            } while (cursor.moveToNext());
        }

        // close db connection
        db.close();

        // return todos list
        return clients;
    }
}
