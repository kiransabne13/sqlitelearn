package me.codeandroid.sqllearn.database.model;

public class Client {
    public static final String TABLE_NAME = "ClientTable";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ADDRESS1 = "address1";
    public static final String COLUMN_ADDRESS2 = "address2";
    public static final String COLUMN_ADDRESS3 = "address3";
    public static final String COLUMN_MOBILE = "mobile";
    public static final String COLUMN_PHONENO = "phoneno";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_PERMIT = "permit";
    public static final String COLUMN_EXPIRY = "expiry";
    public static final String COLUMN_CARD = "card";
    public static final String COLUMN_ONLINE = "online";
    public static final String COLUMN_COMMISSION = "commission";

    private int id;
    private String name, address1, address2, address3, mobile, phoneno, code, permit, expiry, card, online, commission;


    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_ADDRESS1 + " TEXT,"
                    + COLUMN_ADDRESS2 + " TEXT,"
                    + COLUMN_ADDRESS3 + " TEXT,"
                    + COLUMN_MOBILE + " TEXT,"
                    + COLUMN_PHONENO + " TEXT,"
                    + COLUMN_CODE + " TEXT,"
                    + COLUMN_PERMIT + " TEXT,"
                    + COLUMN_EXPIRY + " TEXT,"
                    + COLUMN_CARD + " TEXT,"
                    + COLUMN_ONLINE + " TEXT,"
                    + COLUMN_COMMISSION + " TEXT"
                    + ")";

    public Client() {
    }

    public Client(int id, String name, String address1, String address2, String address3, String mobile, String phoneno, String code, String permit, String expiry, String card, String online, String commission) {
        this.id = id;
        this.name = name;
        this.address1 = address1;
        this.address2 = address2;
        this.address3 = address3;
        this.mobile = mobile;
        this.phoneno = phoneno;
        this.code = code;
        this.permit = permit;
        this.expiry = expiry;
        this.card = card;
        this.online = online;
        this.commission = commission;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2){
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3){
        this.address3 = address3;
    }

    public String getMobile(){
        return mobile;
    }

    public void setMobile(String mobile){
        this.mobile = mobile;
    }

    public String getPhoneno(){
        return phoneno;
    }

    public void setPhoneno(String string){
        this.phoneno = phoneno;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }
}

