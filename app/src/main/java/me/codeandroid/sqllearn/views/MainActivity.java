package me.codeandroid.sqllearn.views;

//adb exec-out run-as package.name cat databases/file > newOutFileName

//TODO inserting string values in sqlitedb is working now, we have to assign a button on screen and have a recyclerview on bottom of screen,
//Implementation is every row in db is assigned as button, and on button pressed, add the row values in recyclerview.

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import me.codeandroid.sqllearn.R;
import me.codeandroid.sqllearn.database.DatabaseHelper;
import me.codeandroid.sqllearn.database.model.Client;
import me.codeandroid.sqllearn.utils.RecyclerTouchListener;

public class MainActivity extends AppCompatActivity {

    private ClientsAdapter clientsAdapter;
    private List<Client> clientsList = new ArrayList<>();
    private DatabaseHelper DbHelper;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        DbHelper = new DatabaseHelper(this);
        createClient("nameValue", "address1", "address2", "address3", "mobile", "phone", "0", "0", "0", "0", "0", "100");

        clientsList.addAll(DbHelper.getAllClients()); // gets all clientsLists from DB

        clientsAdapter = new ClientsAdapter(this, clientsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
     //   recyclerView.addItemDecoration(new MyItemDivider(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(clientsAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(MainActivity.this, "POSITION :" + position, Toast.LENGTH_SHORT).show();
                clientsList.get(position);
                System.out.println(recyclerView.findViewHolderForAdapterPosition(position));
                Log.d("RECYCLERVIEW",recyclerView.getLayoutManager().findViewByPosition(position).toString());
            }
        }));


    }

    private void createClient(String nameValue, String address1, String address2, String address3, String mobile, String phone, String s, String s1, String s2, String s3, String s4, String s5) {
        //get Writeable Database
     //   If you want the View, make sure to access the itemView property of the ViewHolder like so: myRecyclerView.findViewHolderForAdapterPosition(pos).itemView;



        long id = DbHelper.insert(nameValue, address1, address2, address3, mobile, phone, s, s1, s2, s3, s4, s5);

        System.out.println(id);
    }
}
