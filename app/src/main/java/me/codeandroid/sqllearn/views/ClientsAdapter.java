package me.codeandroid.sqllearn.views;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import me.codeandroid.sqllearn.R;
import me.codeandroid.sqllearn.database.model.Client;

public class ClientsAdapter extends RecyclerView.Adapter<ClientsAdapter.MyViewHolder> {
    private Context context;
    private List<Client> clientsList;

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.client_list_row,  viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        Client client = clientsList.get(position);

        myViewHolder.name.setText(client.getName());
        myViewHolder.columnid.setText(client.getAddress1());
        myViewHolder.mobile.setText(client.getMobile());

    }

    @Override
    public int getItemCount() {
        return clientsList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView columnid;
        public TextView mobile;

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(), "View Holder Click " + name, Toast.LENGTH_SHORT).show();
                }
            });
            name = itemView.findViewById(R.id.name);
            columnid = itemView.findViewById(R.id.columnid);
            mobile = itemView.findViewById(R.id.mobile);

        }
    }

    public ClientsAdapter(Context context, List<Client> clientsList) {
        this.context = context;
        this.clientsList = clientsList;
    }
}
